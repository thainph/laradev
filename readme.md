# Laradev
Development environment for Laravel base on Docker. 
It supports multi versions of Laravel (5.x, 8.x, 10.x) and include some tools like phpMyAdmin, RabbitMQ, Mailhog.
## Supported laravel versions
- Laravel 11.x
  ```
  Nginx 1.19
  MySQL 8
  PHP 8.3
  Redis 7.2
  ```
- Laravel 10.x
  ```
  Nginx 1.19
  MySQL 8
  PHP 8.1
  Redis 7.2
  ```
- Laravel 8.x
  ```
  Nginx 1.19
  MySQL 8
  PHP 7.4
  Redis 7.2
  ```
- Laravel 5.x
  ```
  Nginx 1.19
  MySQL 5.7
  PHP 7.3
  Redis 6
  ```
## Supported OS:
  - Linux
  - macOS (Apple chip)
  
## Installation
1 - Install Docker & Docker Compose
- For macOS:
  ```
  https://www.docker.com/products/docker-desktop/
  ```

- For linux:
  ```
  https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce
  ```
  ```
  https://docs.docker.com/compose/install/
  ```

2 - Run script `install` (run only once)
  ```
  chmod +x install && ./install
  ```

## How to use
You can use laradev anywhere by run `laradev`

![img.png](img.png)

## Customize
You can change config in `.env` file
```
WEBSERVER_HTTP_PORT=80
WEBSERVER_HTTPS_PORT=443

LARAVEL_10_VITE_DEV_PORT=5173
LARAVEL_11_VITE_DEV_PORT=5174

MYSQL_ROOT_PASSWORD=example
MYSQL_TIMEZONE=Asia/Ho_Chi_Minh
MYSQL8_PORT=3308
MYSQL5_PORT=3305

PHPMYADMIN_PORT=3333

REDIS_PORT=6379

RABBITMQ_PORT=5672
RABBITMQ_WEB_PORT=15672
RABBITMQ_DEFAULT_USER=root
RABBITMQ_DEFAULT_PASS=example

MAILHOG_PORT=8025
MAILHOG_SMTP_PORT=1025

```

You can change active services in `db/services.csv` file
## Note

* You may get the error when you access laravel project like this: 

  ```
  The stream or file "/var/www/{project_folder}/storage/logs/laravel.log" could not be opened: failed to open stream: Permission denied
  ```
  Let's change the access permission level of log root directory (storage). It is enough to fix the problem and does not cause any side-effect.
  ```
  sudo chmod -R 777 data/{project_folder}/storage
  ```

* When you're adding a new project, you should import `rootCA.crt` to browser to active SSL

* You may get error when build docker services on mac, like this:
  ```
  error getting credentials - err: exit status 1
  ```
  Let remove `credsStore` in `$HOME/.docker/config.json`
