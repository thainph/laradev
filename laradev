#!/bin/bash

source src/docker
source src/init
source src/navigation
source src/project

# Set Color
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

# Constants
BASE_PATH=$(pwd)
OS='linux'
PROJECT_CSV='db/projects.csv'
SERVICE_CSV='db/services.csv'
FOLDER_VHOSTS='nginx/vhosts'
FOLDER_SSL='nginx/ssl'
FILE_NGINX_CONFIG='nginx/config/default.conf'
FOLDER_SUPERVISORD_CONFIG='php-fpm/config'

# Global
RESULT=()

checkRoot() {
  if [[ "$(id -u)" != "0" ]]; then
    printf "${RED}%s${NC}\n" "Please run script with user root"
    exit
  fi
}

selectProjectFolder() {
  clear
  local options=()
  local selected=0
  local entered=false

  for entry in data/*; do
    name=$(echo "$entry" | sed s/"data\/"//)
    options+=("$name")
  done

  while [ "$entered" = false ]; do
    drawMenu "${options[@]}" "Select project folder: "
    handleSelectMenu
  done

  RESULT=("${options[$selected]}")
}

selectProjectVersion() {
  clear
  local options=("Laravel 5 - 8" "Laravel 10" "Laravel 11")
  local selected=0
  local entered=false

  while [ "$entered" = false ]; do
    drawMenu "${options[@]}" "Select Laravel version: "
    handleSelectMenu
  done

  if [ $selected == 0 ]; then
    RESULT=("laravel8")
  elif [ $selected == 1 ]; then
    RESULT=("laravel10")
  elif [ $selected == 2 ]; then
    RESULT=("laravel11")
  fi
}

selectProjectFromCsv() {
  clear
  local options=()
  local folders=()
  local versions=()
  local selected=0
  local entered=false
  local projectCsv=$1

  while IFS=, read -r domain folder version; do
    options+=("$domain")
    folders+=("$folder")
    versions+=("$version")
  done <"$projectCsv"

  while [ "$entered" = false ]; do
    drawMenu "${options[@]}" "Select project: "
    handleSelectMenu
  done

  RESULT=("${options[$selected]}" "${folders[$selected]}" "${versions[$selected]}")
}

addProject() {
  clear
  drawLogo
  printf "Enter project domain: "
  read -r webDomain

  if isExistProject "$webDomain" "$PROJECT_CSV"; then
    printf "${RED}%s${NC}\n" "Project already exists"
    exit
  fi

  selectProjectFolder
  local webFolder="${RESULT[0]}"
  selectProjectVersion
  local webVersion="${RESULT[0]}"

  clear
  drawLogo
  echo "Domain: $webDomain"
  echo "Folder: $webFolder"
  echo "Version: $webVersion"
  echo ""
  addWebsiteConfig "$webDomain" "$webFolder" "$webVersion"
  buildDocker $SERVICE_CSV
  startDocker $SERVICE_CSV
}

addWebsiteConfig() {
  local webDomain=$1
  local webFolder=$2
  local webVersion=$3
  appendProjectToCsv "$webDomain" "$webFolder" "$webVersion" "$PROJECT_CSV"
  createVhostConfig "$webDomain" "$webFolder" "$FOLDER_VHOSTS"
  addDomainToFileHosts "$webDomain"
  updateNginxDefaultConfig "$FILE_NGINX_CONFIG" "$PROJECT_CSV"
  updateSupervisorConfig "$FOLDER_SUPERVISORD_CONFIG" "$webVersion" "$PROJECT_CSV"
  generateSslCert "$webDomain" "$webFolder" "$webVersion" "$FOLDER_SSL"
  chmod -R 777 "$BASE_PATH/data/$webFolder/storage"
}

createDemoWebsites() {
  addWebsiteConfig "laravel8.laradev.web" "laradev" "laravel8"
  addWebsiteConfig "laravel10.laradev.web" "laradev" "laravel10"
  addWebsiteConfig "laravel11.laradev.web" "laradev" "laravel11"
  buildDocker $SERVICE_CSV
  startDocker $SERVICE_CSV
}

switchVersion() {
  clear
  IFS=$'\n' read -r -d '' -a options <<<"$(git tag)"
  local selected=0
  local entered=false

  while [ "$entered" = false ]; do
    drawMenu "${options[@]}" "Select a version: "
    handleSelectMenu
  done

  local version="${options[$selected]}"
  echo "Switching to $version"
  git checkout $version
}

detectOs() {
  clear
  localCurrentOs=$(uname -s)
  if [[ "$localCurrentOs" == "Linux" ]]; then
    OS='linux'
  elif [[ "$localCurrentOs" == "Darwin" ]]; then
    OS='mac'
  else
    printf "${RED}%s${NC}\n" "Unsupported OS"
    exit
  fi
}

reset() {
  clear
  initDockerComposeFiles "$OS" "$BASE_PATH"
  initEnvFile "$BASE_PATH"
  initConfigFiles "$BASE_PATH"
  # shellcheck disable=SC2115
  rm -rf "$FOLDER_VHOSTS"/*
  # shellcheck disable=SC2115
  rm -rf "$FOLDER_SSL"/*
}

redirectToMainMenu() {
  echo ""
  echo ""
  printf "${GREEN}%s${NC}" "Press any key to return to main menu..."
  # shellcheck disable=SC2162
  read -n 1
  echo ""
  mainMenu
}

mainMenu() {
  clear
  local options=("Start services" "Stop services" "Build services" "Add new project" "Host your domain to ngrok" "SSH into project" "SSH into Redis" "SSH into Mysql" "Reset setting" "Switch version" "Create demo websites" "Exit")
  local selected=0
  local entered=false

  while [ "$entered" = false ]; do
    drawMenu "${options[@]}" "Select an option: "
    handleSelectMenu
  done

  if [ $selected == 0 ]; then
    startDocker $SERVICE_CSV
  elif [ $selected == 1 ]; then
    stopDocker $SERVICE_CSV
  elif [ $selected == 2 ]; then
    buildDocker $SERVICE_CSV
  elif [ $selected == 3 ]; then
    addProject
  elif [ $selected == 4 ]; then
    runNgrok
  elif [ $selected == 5 ]; then
    selectProjectFromCsv $PROJECT_CSV
    sshProject "${RESULT[2]}" "${RESULT[1]}"
  elif [ $selected == 6 ]; then
    clear
    sshRedis
  elif [ $selected == 7 ]; then
    clear
    sshMysql
  elif [ $selected == 8 ]; then
    reset
  elif [ $selected == 9 ]; then
    switchVersion
  elif [ $selected == 10 ]; then
    createDemoWebsites
  elif [ $selected == 11 ]; then
    exit
  fi
  redirectToMainMenu
}

checkRoot
detectOs
mainMenu
